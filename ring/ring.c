/**
 * Costa Rica National High Technology Center
 * Advanced Computing Laboratory
 * Santos Dumont HPC School 2024 
 * Instructor: Esteban Meneses, PhD
 * MPI ring computation program.
 */

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define FLAG 7

// Main routine
int main (int argc, char *argv[]){
	int rank, size, number;

	// initialize MPI
	MPI_Init(&argc, &argv);	
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);	
	MPI_Comm_size(MPI_COMM_WORLD, &size);	

	if(rank == (size-1))
		printf("[%d] Total number of ranks: %d\n", rank, size);
	MPI_Barrier(MPI_COMM_WORLD);

	// computing sum of all numbers in the system
	number = 0;
	if(rank == 0){
		MPI_Send(&rank, 1, MPI_INT, (rank+1)%size, FLAG, MPI_COMM_WORLD);
		MPI_Recv(&number, 1, MPI_INT, (rank+size-1)%size, FLAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);	
		printf("[%d] Total sum: %d\n", rank, number);
	} else {
		MPI_Recv(&number, 1, MPI_INT, (rank+size-1)%size, FLAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		number += rank;
		MPI_Send(&number, 1, MPI_INT, (rank+1)%size, FLAG, MPI_COMM_WORLD);
	}

	// verifying sum of all numbers in the system
	MPI_Reduce(&rank, &number, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
	if(rank == 0){
		printf("[%d] Total sum: %d\n", rank, number);
	}

	// finalize MPI
	MPI_Finalize();
	return 0;
}
